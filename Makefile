$(shell mkdir -p build)
VPATH=build:ops:src
SHELL=bash

project=gallery
me=$(shell whoami)
v=latest
webpack=node_modules/.bin/webpack

proxyInclude=$(shell find proxy -type f)
js=$(shell find src -type f)

#########################################
## Begin Phony Rules

all: proxy-image

test: build/index.html
	bash ops/test.sh

push: proxy-image
	docker push $(me)/$(project)_proxy:$v

purge: stop clean

stop:
	docker stack rm $(project)
	@echo; echo -n 'Waiting for the stack to shutdown'
	@while [[ -n "`docker container ls | tail -n +2 | grep $(project)`" ]]; do echo -n '.'; sleep 2; done; echo
	@sleep 2

clean:
	rm build/* 2> /dev/null || true

#########################################
## Begin Real Rules

proxy-image: $(proxyInclude) build/index.html build/dev.html
	docker build -f proxy/Dockerfile -t $(me)/$(project)_proxy:$v -t $(project)_proxy:$v .
	touch build/proxy-image

build/dev.html: bundle.js src/index.html
	cat src/index.html | sed 's/_BUNDLE_LINK_/\/bundle.js/' > build/dev.html

build/index.html: bundle.js src/index.html
	ipfs add build/bundle.js | awk '{print $$2}' > build/bundle.hash
	cat src/index.html | sed 's/_BUNDLE_LINK_/\/ipfs\/'"`cat build/bundle.hash`"'/' > build/index.html

bundle.js: $(js) webpack.client.js node-modules
	$(webpack) --config ops/webpack.client.js

node-modules: package.json
	npm install
	touch build/node-modules
