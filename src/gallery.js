import React from 'react'

import Header from './header'
import Sidebar from './sidebar'
import Content from './content'
import Footer from './footer'

const galleryStyle = {
    margin: '82px 0 0 0',
    width: '100%',
}

class Gallery extends React.Component {
    render() {
        console.log('Rendering')
        return (

            <div>
                <Header />

                <div style={galleryStyle} className="container-fluid"> 
                    <div className="row">
                        <div style={{padding: '0'}} className="col-3">
                            <Sidebar />
                        </div>
                        <div className="col-9">
                            <Content />
                        </div>
                    </div>
                    <div className="row">
                        <Footer />
                    </div>
                </div>
            </div>
        );
    }
}

export default Gallery
