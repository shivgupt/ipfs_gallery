import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery'

import Gallery from './gallery'

ReactDOM.render(
    <Gallery />,
    document.getElementById('root')
);
