import React from 'react'

const sidebarStyle = {
    position: 'fixed',
    zIndex: '-1',
    height: '100%',
    maxWidth: '250px',
    backgroundColor: '#222',
    color: '#ddd',
    padding: '30px',
}

class Sidebar extends React.Component {
    render() {
        return (
            <div id="sidebar" style={sidebarStyle} className="wrapper">
                <div className="sidebar-header">
                    <h2>Timeline</h2>
                </div>
                <ul>
                    <li>Vegas June 2018</li>
                    <li>Zion June 2018</li>
                    <li>Wedding June 2018</li>
                    <li>Crater Lake November 2017</li>
                </ul>
        </div>
    )
}
}

export default Sidebar
