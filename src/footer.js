import React from 'react'

const footerStyle = {
    width: '100%',
    backgroundColor: '#111',
    position: 'fixed',
    bottom: '0',
}

class Footer extends React.Component {
    render() {
        return (
            <div style={footerStyle}>
                <nav className="navbar navbar-dark bg-dark">
                    <div className="navbar-brand">
                        <h5>Contact: admin@email.com</h5>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Footer
