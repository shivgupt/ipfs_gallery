import React from 'react'

const headerStyle = {
    zIndex: '1',
    width: '100%',
    backgroundcolor: '#222',
}

class Header extends React.Component {
    render() {
        return (
            <header style={headerStyle} className="fixed-top">
                <nav className="navbar navbar-dark bg-dark">
                    <div className="navbar-brand">
                        <h1>Bo and Shivani's Gallery </h1>
                    </div>
                </nav>
            </header>
        )
    }
}

export default Header
