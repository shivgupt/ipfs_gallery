#!/bin/bash

########################################
# Set some important variables

me=`whoami` # docker.io username
v=latest
project=gallery
local_port=4002
container_port=4001

[[ -n "$DOMAINNAME" ]] || DOMAINNAME=localhost
[[ -n "$EMAIL" ]] || EMAIL=nothanks@example.com

########################################
echo "Deploying blog in environment: me=$me v=$v DOMAINNAME=$DOMAINNAME EMAIL=$EMAIL"
sleep 1

if [[ -z "`ps -e | grep ipfs | grep daemon`" ]]
then
	echo "Starting local ipfs daemon... "
    ipfs config Addresses.Swarm --json '["/ip4/0.0.0.0/tcp/'"$local_port"'","/ip6/::/tcp/'"$local_port"'"]'
    ipfs daemon &
    sleep 5
fi

local_address="/ip4/127.0.0.1/tcp/$local_port/ipfs/`ipfs id --format="<id>"`"
echo "Local ipfs daemon detected with address: $local_address"

# Specify & update docker images
if [[ "$DOMAINNAME" == "localhost" ]]
then
    proxy="${project}_proxy:$v"
else
    proxy="$me/${project}_proxy:$v"
fi
ipfs="ipfs/go-ipfs"

if [[ "$DOMAINNAME" != "localhost" ]]
then
    docker pull $proxy
    docker pull $ipfs
fi

mkdir -p /tmp/$project
cat -> /tmp/$project/docker-compose.yml <<EOF
version: '3.4'

volumes:
  letsencrypt:
  devcerts:
  ipfs:

services:

  proxy:
    image: $proxy
    deploy:
      mode: global
    depends_on:
      - ipfs
    volumes:
      - devcerts:/etc/devcerts
      - letsencrypt:/etc/letsencrypt
    environment:
      - DOMAINNAME=$DOMAINNAME
      - EMAIL=$EMAIL
    ports:
      - "80:80"
      - "443:443"

  ipfs:
    image: $ipfs
    deploy:
      mode: global
    volumes:
      - ipfs:/data/ipfs
    ports:
      - "$container_port:4001"
EOF

docker stack deploy -c /tmp/$project/docker-compose.yml $project
rm -rf /tmp/$project

echo -n "Waiting for the $project to wake up.."
number_of_containers=2
while true
do
    sleep 3
    if [[ "`docker container ls | grep $project | wc -l | sed 's/ //g'`" == "$number_of_containers" ]]
    then
        echo " Good Morning!"
        sleep 10
        if [[ "`docker container ls | grep $project | wc -l | sed 's/ //g'`" == "$number_of_containers" ]]
        then
            break
        fi
        echo -n "."
    else
        echo -n "."
    fi
done

name=${project}_ipfs
id=$(docker service ps -q $name | head -n1)
long_id=$(docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $id)

docker exec -it $long_id ipfs bootstrap add $local_address
ipfs bootstrap add "/ip4/127.0.0.1/tcp/$container_port/ipfs/`docker exec -it $long_id ipfs id --format="<id>"`"
