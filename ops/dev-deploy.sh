#!/bin/bash

########################################
# Set some important variables

me=`whoami` # docker.io username
v=latest
project=gallery

[[ -n "$DOMAINNAME" ]] || DOMAINNAME=localhost
[[ -n "$EMAIL" ]] || EMAIL=nothanks@example.com

########################################
echo "Deploying blog in environment: me=$me v=$v DOMAINNAME=$DOMAINNAME EMAIL=$EMAIL"
sleep 1

proxy="${project}_proxy:$v"
ipfs="ipfs/go-ipfs"

mkdir -p /tmp/$project
cat -> /tmp/$project/docker-compose.yml <<EOF
version: '3.4'

volumes:
  letsencrypt:
  devcerts:

services:

  proxy:
    image: $proxy
    deploy:
      mode: global
    depends_on:
      - ipfs
    volumes:
      - devcerts:/etc/devcerts
      - letsencrypt:/etc/letsencrypt
      - "`pwd`/build:/var/www/html"
    environment:
      - DOMAINNAME=$DOMAINNAME
      - EMAIL=$EMAIL
    ports:
      - "80:80"
      - "443:443"

  ipfs:
    image: $ipfs
    deploy:
      mode: global
    ports:
      - "$container_port:4001"
EOF

docker stack deploy -c /tmp/$project/docker-compose.yml $project
rm -rf /tmp/$project
