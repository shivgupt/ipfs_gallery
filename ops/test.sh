#!/bin/bash

hash="`cat build/bundle.hash`"
if [[ -n "`curl --head --max-time 5 --insecure https://localhost/ipfs/$hash 2> /dev/null | head -n 1 | grep 200`"  ]]
then
    echo 'Tests passed!'
    exit 0
else
    echo 'Tests failed!'
    exit 1
fi
