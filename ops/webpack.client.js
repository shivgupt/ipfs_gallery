
const path = require('path');
const webpack = require('webpack');

module.exports = {

  mode: 'development',

  entry: './src/entry.js',

  output: {
    path: path.join(__dirname, '../build'),
    filename: 'bundle.js',
  },

  resolve: {
    extensions: ['.js', '.json'],
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: ['es2015','react'],
            },
        },
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ],
      },
    ],
  },

};
